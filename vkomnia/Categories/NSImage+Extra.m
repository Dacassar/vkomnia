//
//  Extra.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "NSImage+Extra.h"

@implementation NSImage (Extra)

+ (NSImage *)imageWithColor:(NSColor *)color size:(NSSize)size
{
    NSImage *image = [[NSImage alloc] initWithSize:size];
    [image lockFocus];
    [color drawSwatchInRect:NSMakeRect(0, 0, size.width, size.height)];
    [image unlockFocus];
    return image;
}

- (NSData *)PNGRepresentation
{
    // Create a bitmap representation from the current image
    
    [self lockFocus];
    NSBitmapImageRep *bitmapRep = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect(0, 0, self.size.width, self.size.height)];
    [self unlockFocus];
    return [bitmapRep representationUsingType:NSPNGFileType properties:Nil];
}

- (NSData*)JPGRepresentation
{
    NSData *imageData = [self TIFFRepresentation];
    NSBitmapImageRep *imageRep = [NSBitmapImageRep imageRepWithData:imageData];
    NSDictionary *imageProps = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1.0] forKey:NSImageCompressionFactor];
    imageData = [imageRep representationUsingType:NSJPEGFileType properties:imageProps];
    return imageData;
}

- (NSString *)base64Representation
{
    NSData *data = [self JPGRepresentation];
    NSString *base64 = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength|NSDataBase64EncodingEndLineWithLineFeed];
    return base64;
}

@end
