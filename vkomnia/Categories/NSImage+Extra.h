//
//  Extra.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (Extra)
+ (NSImage*)imageWithColor:(NSColor*)color size:(NSSize)size;
- (NSData *)PNGRepresentation;
- (NSString*)base64Representation;
- (NSData*)JPGRepresentation;
@end
