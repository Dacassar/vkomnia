//
//  NSString+Extra.m
//  vkomnia
//
//  Created by Евгений Браницкий on 02.12.15.
//  Copyright © 2015 Yevgeniy Kratko. All rights reserved.
//

#import "NSString+Extra.h"

@implementation NSString (Extra)
- (NSString *)stringUsingEncoding:(NSStringEncoding)encoding
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                 (CFStringRef)self,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                 CFStringConvertNSStringEncodingToEncoding(encoding)));
}
@end
