//
//  NSString+Extra.h
//  vkomnia
//
//  Created by Евгений Браницкий on 02.12.15.
//  Copyright © 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extra)
- (NSString *)stringUsingEncoding:(NSStringEncoding)encoding;
@end
