//
//  NSDate+TimeAgo.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 30.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DateAgo) {
    Day,
    Week,
    Month,
    Year
};

@interface NSDate (TimeAgo)
+ (NSDate*)dateAgo:(DateAgo)ago;
@end
