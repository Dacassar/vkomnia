//
//  NSColor+Flat.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSColor (Flat)

+ (NSColor *) colorFromHexCode:(NSString *)hexString;
+ (NSColor *) turquoiseColor;
+ (NSColor *) greenSeaColor;
+ (NSColor *) emerlandColor;
+ (NSColor *) nephritisColor;
+ (NSColor *) peterRiverColor;
+ (NSColor *) belizeHoleColor;
+ (NSColor *) amethystColor;
+ (NSColor *) wisteriaColor;
+ (NSColor *) wetAsphaltColor;
+ (NSColor *) midnightBlueColor;
+ (NSColor *) sunflowerColor;
+ (NSColor *) tangerineColor;
+ (NSColor *) carrotColor;
+ (NSColor *) pumpkinColor;
+ (NSColor *) alizarinColor;
+ (NSColor *) pomegranateColor;
+ (NSColor *) cloudsColor;
+ (NSColor *) silverColor;
+ (NSColor *) concreteColor;
+ (NSColor *) asbestosColor;

//+ (NSColor *) blendedColorWithForegroundColor:(NSColor *)foregroundColor
//                              backgroundColor:(NSColor *)backgroundColor
//                                 percentBlend:(CGFloat) percentBlend;

@end
