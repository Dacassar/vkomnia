//
//  NSColor+Flat.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "NSColor+Flat.h"

@implementation NSColor (Flat)
// Thanks to http://stackoverflow.com/questions/3805177/how-to-convert-hex-rgb-color-codes-to-NSColor
+ (NSColor *) colorFromHexCode:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if ([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [NSColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (NSColor *) turquoiseColor {
    static NSColor *turquoise = nil;
    static dispatch_once_t dispatchToken;
    
    dispatch_once(&dispatchToken, ^{
        turquoise = [NSColor colorFromHexCode:@"1ABC9C"];
    });
    
    return turquoise;
}

+ (NSColor *) greenSeaColor {
    static NSColor *greenSea = nil;
    static dispatch_once_t greenToken;
    
    dispatch_once(&greenToken, ^{
        greenSea = [NSColor colorFromHexCode:@"16A085"];
    });
    
    return greenSea;
}

+ (NSColor *) emerlandColor {
    static NSColor *emerald = nil;
    static dispatch_once_t emeraldToken;
    
    dispatch_once(&emeraldToken, ^{
        emerald = [NSColor colorFromHexCode:@"2ECC71"];
    });
    
    return emerald;
}

+ (NSColor *) nephritisColor {
    static NSColor *nephritis = nil;
    static dispatch_once_t nephritisToken;
    
    dispatch_once(&nephritisToken, ^{
        nephritis = [NSColor colorFromHexCode:@"27AE60"];
    });
    
    return nephritis;
}

+ (NSColor *) peterRiverColor {
    static NSColor *peterRiver = nil;
    static dispatch_once_t peterToken;
    
    dispatch_once(&peterToken, ^{
        peterRiver = [NSColor colorFromHexCode:@"#3498DB"];
    });
    
    return peterRiver;
}

+ (NSColor *) belizeHoleColor {
    static NSColor *tripToBelize = nil; // Let's cook!
    static dispatch_once_t belizeToken;
    
    dispatch_once(&belizeToken, ^{
        tripToBelize = [NSColor colorFromHexCode:@"2980B9"];
    });
    
    return tripToBelize;
}

+ (NSColor *) amethystColor {
    static NSColor *amethyst = nil;
    static dispatch_once_t amethystToken;
    
    dispatch_once(&amethystToken, ^{
        amethyst = [NSColor colorFromHexCode:@"9B59B6"];
    });
    
    return amethyst;
}

+ (NSColor *) wisteriaColor {
    static NSColor *wisteria = nil;
    static dispatch_once_t wisteriaToken;
    
    dispatch_once(&wisteriaToken, ^{
        wisteria = [NSColor colorFromHexCode:@"8E44AD"];
    });
    
    return wisteria;
}

+ (NSColor *) wetAsphaltColor {
    static NSColor *asphalt = nil;
    static dispatch_once_t asphaltToken;
    
    dispatch_once(&asphaltToken, ^{
        asphalt = [NSColor colorFromHexCode:@"34495E"];
    });
    
    return asphalt;
}

+ (NSColor *) midnightBlueColor {
    static NSColor *midnightBlue = nil;
    static dispatch_once_t midnightBlueToken;
    
    dispatch_once(&midnightBlueToken, ^{
        midnightBlue = [NSColor colorFromHexCode:@"2C3E50"];
    });
    
    return midnightBlue;
}

+ (NSColor *) sunflowerColor {
    static NSColor *sunflower = nil;
    static dispatch_once_t sunflowerToken;
    
    dispatch_once(&sunflowerToken, ^{
        sunflower = [NSColor colorFromHexCode:@"F1C40F"];
    });
    
    return sunflower;
}

+ (NSColor *) tangerineColor {
    static NSColor *tangerine = nil;
    static dispatch_once_t tangerineToken;
    
    dispatch_once(&tangerineToken, ^{
        tangerine = [NSColor colorFromHexCode:@"F39C12"];
    });
    
    return tangerine;
}

+ (NSColor *) carrotColor {
    static NSColor *carrot = nil;
    static dispatch_once_t carrotToken;
    
    dispatch_once(&carrotToken, ^{
        carrot = [NSColor colorFromHexCode:@"E67E22"];
    });
    
    return carrot;
}

+ (NSColor *) pumpkinColor {
    static NSColor *pumpkin = nil;
    static dispatch_once_t pumpkinToken;
    
    dispatch_once(&pumpkinToken, ^{
        pumpkin = [NSColor colorFromHexCode:@"D35400"];
    });
    
    return pumpkin;
}

+ (NSColor *) alizarinColor {
    static NSColor *alizarin = nil;
    static dispatch_once_t alizarinToken;
    
    dispatch_once(&alizarinToken, ^{
        alizarin = [NSColor colorFromHexCode:@"E74C3C"];
    });
    
    return alizarin;
}

+ (NSColor *) pomegranateColor {
    static NSColor *pomegranate = nil;
    static dispatch_once_t pomegranateToken;
    
    dispatch_once(&pomegranateToken, ^{
        pomegranate = [NSColor colorFromHexCode:@"C0392B"];
    });
    
    return pomegranate;
}

+ (NSColor *) cloudsColor {
    static NSColor *clouds = nil;
    static dispatch_once_t cloudsToken;
    
    dispatch_once(&cloudsToken, ^{
        clouds = [NSColor colorFromHexCode:@"ECF0F1"];
    });
    
    return clouds;
}

+ (NSColor *) silverColor {
    static NSColor *silver = nil;
    static dispatch_once_t silverToken;
    
    dispatch_once(&silverToken, ^{
        silver = [NSColor colorFromHexCode:@"BDC3C7"];
    });
    
    return silver;
}

+ (NSColor *) concreteColor {
    static NSColor *concrete = nil;
    static dispatch_once_t concreteToken;
    
    dispatch_once(&concreteToken, ^{
        concrete = [NSColor colorFromHexCode:@"95A5A6"];
    });
    
    return concrete;
}

+ (NSColor *) asbestosColor {
    static NSColor *asbestos = nil;
    static dispatch_once_t asbestosToken;
    
    dispatch_once(&asbestosToken, ^{
        asbestos = [NSColor colorFromHexCode:@"7F8C8D"];
    });
    
    return asbestos;
}
/*
+ (NSColor *) blendedColorWithForegroundColor:(NSColor *)foregroundColor
                              backgroundColor:(NSColor *)backgroundColor
                                 percentBlend:(CGFloat) percentBlend {
    CGFloat components[3];
    [foregroundColor getRed:&components[0] green:&components[1] blue:&components[2] alpha:NULL];
    if (components != NULL) {
        [foregroundColor getWhite:&onWhite alpha:nil];
        onRed = onWhite;
        onBlue = onWhite;
        onGreen = onWhite;
    }
    if (![backgroundColor getRed:&offRed green:&offGreen blue:&offBlue alpha:nil]) {
        [backgroundColor getWhite:&offWhite alpha:nil];
        offRed = offWhite;
        offBlue = offWhite;
        offGreen = offWhite;
    }
    newRed = onRed * percentBlend + offRed * (1-percentBlend);
    newGreen = onGreen * percentBlend + offGreen * (1-percentBlend);
    newBlue = onBlue * percentBlend + offBlue * (1-percentBlend);
    return [NSColor colorWithRed:newRed green:newGreen blue:newBlue alpha:1.0];
}*/
@end
