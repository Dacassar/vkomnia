//
//  NSDate+TimeAgo.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 30.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "NSDate+TimeAgo.h"

@implementation NSDate (TimeAgo)

+ (NSDate *)dateAgo:(DateAgo)ago
{
    NSCalendar *cal = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    NSDate *result = nil;
    switch (ago) {
        case Day: {
            [components setDay:components.day-1];
            result = [cal dateFromComponents:components];
            break;
        }
        case Week: {
            [components setDay:components.day - 7];
            result = [cal dateFromComponents:components];
            break;
        }
        case Month: {
            [components setMonth:components.month - 1];
            result = [cal dateFromComponents:components];
            break;
        }
        case Year: {
            [components setYear:components.year - 1];
            result = [cal dateFromComponents:components];
            break;
        }
        default:break;
    }
    return result;
}

@end
