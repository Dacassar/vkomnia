//
//  VKConnector.m
//
//  Created by Andrew Shmig on 18.12.12.
//
//
// Copyright (c) 2013 Andrew Shmig
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
// FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "VKConnector.h"
#import "VKAccessToken.h"
#import "NSString+encodeURL.h"

@import WebKit;

#define MARGIN_WIDTH 25.0 // ширина отступа от границ экрана
#define MARGIN_HEIGHT 50.0 // высота отступа
#define DEFAULT_TIMEOUTINTERVAL 5 * 60.0 // 5 минут

@interface VKConnector()
@property (strong, nonatomic) NSString *settings;
@property (strong, nonatomic) NSString *redirectURL;
@property (strong, nonatomic) WebView *innerWebView;
@property (strong, nonatomic) NSWindow *loginWindow;
@end

@implementation VKConnector

#pragma mark - Init methods

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static VKConnector *instanceVKConnector = nil;
    dispatch_once(&onceToken, ^{
        instanceVKConnector = [[VKConnector alloc] init];
    });
    return instanceVKConnector;
}

#pragma mark - VKConnector public methods

- (void)startWithAppID:(NSString *)appID
            permissons:(NSArray *)permissions
{
    _permissions = permissions;
    _appID = appID;

    _settings = [self.permissions componentsJoinedByString:@","];
    _redirectURL = @"https://oauth.vk.com/blank.html";

    if (self.loginWindow == nil) {
        NSWindow *window = [[NSWindow alloc] initWithContentRect:NSMakeRect(0, 0, 265, 424) styleMask:NSTitledWindowMask backing:NSBackingStoreBuffered defer:NO];
        WebView *webview = [[WebView alloc] initWithFrame:NSMakeRect(0, 0, 265, 424)];
        [window setContentView:webview];
        [webview setFrameLoadDelegate:self];
        
        [self setInnerWebView:webview];
        [self setLoginWindow:window];
    }
//    преобразование словаря параметров в строку параметров
    NSDictionary *params = @{@"client_id"     : self.appID,
                             @"redirect_uri"  : _redirectURL,
                             @"scope"         : _settings,
                             @"response_type" : @"token",
                             @"display"       : @"touch"};
    NSMutableString *urlAsString = [[NSMutableString alloc] init];
    NSMutableArray *urlParams = [[NSMutableArray alloc] init];

    [urlAsString appendString:@"https://oauth.vk.com/authorize?"];
    [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
    {
        [urlParams addObject:[NSString stringWithFormat:@"%@=%@", key, obj]];
    }];
    [urlAsString appendString:[urlParams componentsJoinedByString:@"&"]];

//    запрос на страницу авторизации приложения
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

//    отображаем попап, если токен недействителен
    VKAccessToken *token = [VKAccessToken sharedToken];
    BOOL isLoad = [token load];
    BOOL isValid = [token isValid];
    if (!isLoad || !isValid) {
        [[self.innerWebView mainFrame] loadRequest:request];
        if ([self.delegate respondsToSelector:@selector(VKConnector:willHideModalWindow:)]) {
            [self.delegate VKConnector:self willShowModalWindow:self.loginWindow];
        }
        if ([self.delegate isKindOfClass:[NSViewController class]]) {
            __weak __block void (^weakShowBlock)();
            __weak typeof(self) weakSelf = self;
            void (^showBlock)() = ^{
                NSWindow *window = [[(NSViewController*)weakSelf.delegate view] window];
                if (window) {
                    [window beginSheet:weakSelf.loginWindow completionHandler:nil];
                }
                else {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        weakShowBlock();
                    });
                }
            };
            weakShowBlock = showBlock;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0), dispatch_get_main_queue(), showBlock);
        }
    }
}

- (void)logout
{
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];

    for (NSHTTPCookie *cookie in cookies) {
        if (NSNotFound != [cookie.domain rangeOfString:@"vk.com"].location) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage]
                                  deleteCookie:cookie];
        }
    }
}

- (id)performVKMethod:(NSString *)methodName
              options:(NSDictionary *)options
                error:(NSError **)error
{
    BOOL isValid = [[VKAccessToken sharedToken] isValid];
    if (!isValid) {

        if ([self.delegate respondsToSelector:@selector(VKConnector:accessTokenInvalidated:)])
            [self.delegate VKConnector:self accessTokenInvalidated:[VKAccessToken sharedToken]];
        return nil;
    }

//    формируем УРЛ на который буде отправлен запрос
    NSMutableString *fullRequestURL = [NSMutableString stringWithFormat:@"%@%@",
                                                                        kVkontakteAPIURL,
                                                                        methodName];

    [fullRequestURL appendString:@"?"];

    [options enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
    {
        [fullRequestURL appendFormat:@"%@=%@&",
                                     key,
                                     [[obj description] encodeURL]];
    }];

    [fullRequestURL appendFormat:@"access_token=%@", [VKAccessToken sharedToken].token];

    NSURL *url = [NSURL URLWithString:fullRequestURL];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];

    return [self performVKRequest:urlRequest];
}

- (id)performVKRequest:(NSMutableURLRequest *)request
{
    //    отправка запроса
    NSHTTPURLResponse *httpResponse;
    NSError *requestError;
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&httpResponse
                                                         error:&requestError];

//    во время запроса произошла ошибка
    if (nil != requestError) {
        if ([self.delegate respondsToSelector:@selector(VKConnector:connectionErrorOccured:)])
            [self.delegate VKConnector:self
                connectionErrorOccured:requestError];

        return nil;
    }

    NSError *jsonParsingError;
    id jsonResponse = [NSJSONSerialization JSONObjectWithData:response
                                                      options:NSJSONReadingMutableContainers
                                                        error:&jsonParsingError];

//    ошибка парсинга ответа сервера
    if (nil != jsonParsingError) {
        if ([self.delegate respondsToSelector:@selector(VKConnector:parsingErrorOccured:)])
            [self.delegate VKConnector:self
                   parsingErrorOccured:jsonParsingError];

        return nil;
    }

//    результат
    return jsonResponse;
}

- (id)uploadFile:(NSData *)file
            name:(NSString *)fileName
             URL:(NSURL *)url
     contentType:(NSString *)contentType
       fieldName:(NSString *)fieldName
{
    // формируем запрос
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSString *boundary = [[NSProcessInfo processInfo] globallyUniqueString];
    NSMutableData *body = [NSMutableData data];

    [request setTimeoutInterval:DEFAULT_TIMEOUTINTERVAL];

    // тело
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                                dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",
                                                 fieldName, fileName]
                                                 dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",
                                                 contentType]
                                                 dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:file];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary]
                                dataUsingEncoding:NSUTF8StringEncoding]];

    [request setHTTPBody:body];

    // заголовки
    [request setHTTPMethod:@"POST"];

    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=\"%@\"",
                                                 boundary]
   forHTTPHeaderField:@"Content-Type"];

    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]]
   forHTTPHeaderField:@"Content-Length"];

    // отправка запроса
    return [self performVKRequest:request];
}

#pragma mark - WebView delegate methods

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
    NSString *str = [sender mainFrameURL];
//    BOOL respond = [self.vkFbDelegate respondsToSelector:@selector(getToken:userID:service:)];
    if ([str rangeOfString:@"access_token"].location != NSNotFound) {
        NSArray *parts = [str componentsSeparatedByString:@"&"];
//            пользователь одобрил наше приложение, парсим полученные данные
        NSString *access_token = [parts[0] componentsSeparatedByString:@"="][1];
        NSTimeInterval expiration_time = [[parts[1] componentsSeparatedByString:@"="][1] doubleValue];
        NSUInteger user_id = [[parts[2] componentsSeparatedByString:@"="][1] integerValue];

//        VKAccessToken *token = [[VKAccessToken alloc] initWithUserID:user_id accessToken:access_token expirationTime:expiration_time permissions:self.permissions];
//        [self setAccessToken:token];
        [[VKAccessToken sharedToken] setCredentials:@{@"userID":@(user_id), @"token":access_token, @"exp":@(expiration_time), @"permissions":self.permissions}];
        [[VKAccessToken sharedToken] save];
        //            уведомляем программиста, что токен был обновлён
        if ([self.delegate respondsToSelector:@selector(VKConnector:accessTokenRenewalSucceeded:)]) {
            [self.delegate VKConnector:self accessTokenRenewalSucceeded:[VKAccessToken sharedToken]];
        }
        else {
        //            пользователь отказался авторизовать приложение
        //            не удалось обновить/получить токен доступа
            if ([self.delegate respondsToSelector:@selector(VKConnector:accessTokenRenewalFailed:)]) {
                [self.delegate VKConnector:self accessTokenRenewalFailed:nil];
            }
        }
    }
    if ([str hasPrefix:@"https://oauth.vk.com/blank.html"]) {
        if ([self.delegate respondsToSelector:@selector(VKConnector:willHideModalWindow:)]) {
            [self.delegate VKConnector:self willHideModalWindow:self.loginWindow];
        }
        [[[(NSViewController*)self.delegate view] window] endSheet:self.loginWindow returnCode:NSModalResponseOK];
    }
}

/*
- (BOOL)           webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
            navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *url = [[request URL] absoluteString];

    if ([url hasPrefix:_redirectURL]) {
        NSString *query_string = [url componentsSeparatedByString:@"#"][1];

//        проверяем одобрил ли пользователь наше приложение или нет
        if ([query_string hasPrefix:@"access_token"]) {
            NSArray *parts = [query_string componentsSeparatedByString:@"&"];

//            пользователь одобрил наше приложение, парсим полученные данные
            NSString *access_token = [parts[0] componentsSeparatedByString:@"="][1];
            NSTimeInterval expiration_time = [[parts[1] componentsSeparatedByString:@"="][1] doubleValue];
            NSUInteger user_id = [[parts[2] componentsSeparatedByString:@"="][1] unsignedIntValue];

            _accessToken = [[VKAccessToken alloc]
                                           initWithUserID:user_id
                                              accessToken:access_token
                                           expirationTime:expiration_time
                                              permissions:[_settings componentsSeparatedByString:@","]];

//            сохраняем токен доступа
            [_accessToken save];

//            уведомляем программиста, что токен был обновлён
            if ([self.delegate respondsToSelector:@selector(VKConnector:accessTokenRenewalSucceeded:)])
                [self.delegate VKConnector:self
               accessTokenRenewalSucceeded:_accessToken];

        } else {
//            пользователь отказался авторизовать приложение
//            не удалось обновить/получить токен доступа
            if ([self.delegate respondsToSelector:@selector(VKConnector:accessTokenRenewalFailed:)])
                [self.delegate VKConnector:self
                  accessTokenRenewalFailed:nil];
        }
    }

    if ([url hasPrefix:@"https://oauth.vk.com/blank.html"]) {
        if ([self.delegate respondsToSelector:@selector(VKConnector:willHideModalView:)])
            [self.delegate VKConnector:self
                     willHideModalView:[KGModal sharedInstance]];

        [[KGModal sharedInstance] hideAnimated:YES];
    }

    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activityIndicator stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_activityIndicator startAnimating];
}
*/
@end
