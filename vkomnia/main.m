//
//  main.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 23.12.14.
//  Copyright (c) 2014 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
