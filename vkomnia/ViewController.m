//
//  ViewController.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 23.12.14.
//  Copyright (c) 2014 Yevgeniy Kratko. All rights reserved.
//

#import "ViewController.h"

#import "VORoundedImageView.h"
#import "VOGroupCellView.h"
#import "VOView.h"
#import "VOTableRowView.h"

#import "VOGroupViewController.h"

@import SwiftyVK;

@interface ViewController () <NSTableViewDataSource, NSTableViewDelegate>
@property (weak) IBOutlet VORoundedImageView *avatarView;
@property (weak) IBOutlet NSTextField *nameLabel;
@property (weak) IBOutlet NSTableView *groupsTableView;
@property (strong, nonatomic) NSArray *groups;
@end

@implementation ViewController

static NSString *const APP_ID = @"3413489";
static NSString *const APP_PERMISSIONS = @"friends,photos,groups,messages,pages,offline,notifications,wall,stats";

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    [(VOView*)self.view setBackgroundColor:[NSColor cloudsColor]];

    if ([self updateUserInfo]) {
        [self loadUserGroups];
    }
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)VKConnector:(VKConnector *)connector accessTokenRenewalSucceeded:(VKAccessToken *)accessToken
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self updateUserInfo];
}

- (void)VKConnector:(VKConnector *)connector parsingErrorOccured:(NSError *)error
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)VKConnector:(VKConnector *)connector accessTokenRenewalFailed:(VKAccessToken *)accessToken
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)VKConnector:(VKConnector *)connector accessTokenInvalidated:(VKAccessToken *)accessToken
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [[VKConnector sharedInstance] startWithAppID:APP_ID permissons:[APP_PERMISSIONS componentsSeparatedByString:@","]];
}


- (BOOL)updateUserInfo
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    VKUser *user = [VKUser currentUser];
    if (!user) {
        [[VKConnector sharedInstance] startWithAppID:APP_ID permissons:[APP_PERMISSIONS componentsSeparatedByString:@","]];
        return NO;;
    }
    NSDictionary *info = [[[user info] valueForKey:@"response"] objectAtIndex:0];
    if (!info) {
        return NO;
    }
    NSString *fName = [info valueForKeyPath:@"first_name"];
    NSString *lName = [info valueForKeyPath:@"last_name"];
    NSString *nickname = [info valueForKeyPath:@"nickname"];
    if (nickname) {
        nickname = [NSString stringWithFormat:@" %@ ", nickname];
    }
    
    [self.nameLabel setStringValue:[NSString stringWithFormat:@"%@%@%@", fName, nickname, lName]];
    [self.avatarView setRounded:YES];
    [self.avatarView setImageURL:[NSURL URLWithString:[info valueForKey:@"photo_medium"]]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSInteger count = 1;
        NSInteger offset = 0;
        NSMutableArray *users = [NSMutableArray array];
        while (count > offset) {
            NSString *script = [NSString stringWithFormat:@"var offset=%ld;\nvar _acl=25;\nvar test=API.messages.getDialogs({count:1}); _acl=_acl-1;\nvar count=test.count;\nvar users=[];\n\nwhile (_acl > 1 && offset < count) {\n\tvar buffer=API.messages.getDialogs({count:200,offset:offset}); _acl=_acl-1;\n\tcount=buffer.count;\n\toffset=offset+200;\n\tusers=users+buffer.items@.message@.user_id;\n}\nvar result = {count:count, offset:offset, items:users};\nreturn result;", offset];
            NSError *error = nil;
            id res = [[VKConnector sharedInstance] performVKMethod:@"execute" options:@{@"code":script, @"v":@"5.27"} error:&error];
            if ((res && !error) || ![res valueForKey:@"error"]) {
                [users addObjectsFromArray:[res valueForKeyPath:@"response.items"]];
                offset = [[res valueForKeyPath:@"response.offset"] integerValue];
                count = [[res valueForKeyPath:@"response.count"] integerValue];
            }
            else {
                break;
            }
        }
        setInvitedUsers((NSArray*)users);
    });
    
    return YES;
}

- (void)loadUserGroups
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        VKGroups *groups = [[VKUser currentUser] groups];
        NSDictionary *groupList = [groups listCustomOptions:@{@"filter":@"admin,moder,editor", @"extended":@(YES)}];
        if (groupList) {
            NSMutableArray *arr = [NSMutableArray array];
            [[groupList valueForKey:@"response"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([obj isKindOfClass:[NSDictionary class]]) {
                    [arr addObject:obj];
                }
            }];
            [weakSelf setGroups:[NSArray arrayWithArray:arr]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.groupsTableView reloadData];
            });
        }
    });
}

#pragma mark - TableView Delegate & Datasource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [self.groups count];
}

- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row
{
    return 45;
}

- (NSTableRowView *)tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row
{
    static NSString* const kRowIdentifier = @"RowView";
    VOTableRowView *rowView = [tableView makeViewWithIdentifier:kRowIdentifier owner:self];
    if (!rowView) {
        rowView = [[VOTableRowView alloc] initWithFrame:NSZeroRect];
        rowView.identifier = kRowIdentifier;
    }
    [rowView setSelectionColor:[NSColor greenSeaColor]];
    return rowView;
}

- (NSView*)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    VOGroupCellView *view = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    NSDictionary *group = [self.groups objectAtIndex:row];
    [view.textField setStringValue:[group objectForKey:@"name"]];
    [view.screenNameLabel setStringValue:[group valueForKey:@"screen_name"]];
    [view.imageView setImageURL:[NSURL URLWithString:[group valueForKey:@"photo_medium"]]];
    return view;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    [self performSegueWithIdentifier:@"enterGroup" sender:self];
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender
{
    NSInteger row = [self.groupsTableView selectedRow];
    NSDictionary *group = [self.groups objectAtIndex:row];
    VOGroupViewController *gv = [segue destinationController];
    [gv setGroupInfo:group];
}

@end
