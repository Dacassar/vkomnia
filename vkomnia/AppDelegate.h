//
//  AppDelegate.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 23.12.14.
//  Copyright (c) 2014 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
@property (strong, nonatomic) NSArray *groupMembers;
@property (strong, nonatomic) NSArray *invitedUsers;
@end

