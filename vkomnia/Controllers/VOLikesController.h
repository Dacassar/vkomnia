//
//  VOLikesController.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 19.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class VOFeedPost;
@interface VOLikesController : NSViewController
@property (strong, nonatomic) VOFeedPost *post;
@end
