//
//  VOStatController.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 30.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VOStatController : NSViewController
@property (assign, nonatomic) NSInteger groupID;
@end
