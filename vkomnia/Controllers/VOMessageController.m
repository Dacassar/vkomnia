//
//  VOMessageController.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 21.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOMessageController.h"
#import "VORoundedImageView.h"
#import "VOLikedItem.h"
#import "VKConnector.h"

NSString *const VOMessageDidSentNotification = @"VOMessageDidSentNotification";
NSString *const VOMessageDidCloseNotification = @"VOMessageDidCloseNotification";

@interface VOMessageController ()
@property (weak) IBOutlet VORoundedImageView *avatarView;
@property (unsafe_unretained) IBOutlet NSTextView *messageView;
- (IBAction)sendMessage:(id)sender;
- (IBAction)closePopover:(id)sender;
@end

@implementation VOMessageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self.avatarView setRounded:YES];
    
    NSInteger spaceIndex = [self.like.name rangeOfString:@" "].location;
    NSString *nameOnly = [self.like.name substringToIndex:spaceIndex];
    NSString *template = [NSString stringWithFormat:@"Доброго времени суток, %@!\nОбратил внимание, что Вам понравился текст Евгении Барановой \"Живаго\" %@, и как администратор, приглашаю Вас в официальную группу Евгении http://vk.com/hoagoa. Если же Вы уже состоите в ней, или это Вам не интересно - извините =)", nameOnly, self.postURL];
    [self.messageView setString:template];
}

- (IBAction)sendMessage:(id)sender
{
    NSError *error = nil;
    id res = [[VKConnector sharedInstance] performVKMethod:kVKMessagesSend options:@{@"user_id":@(self.like.itemID), @"message":self.messageView.string} error:&error];
    if (![res valueForKey:@"error"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:VOMessageDidSentNotification object:self.like];
    }
    else {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setAlertStyle:NSCriticalAlertStyle];
        [alert setInformativeText:[res valueForKeyPath:@"error.error_msg"]];
        [alert setMessageText:@"Something is going wrong..."];
        [alert runModal];
    }
    [self closePopover:self];
}

- (IBAction)closePopover:(id)sender
{
    [self dismissViewController:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:VOMessageDidCloseNotification object:nil];
}
@end
