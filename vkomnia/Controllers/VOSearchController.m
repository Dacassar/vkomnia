//
//  VOSearchController.m
//  
//
//  Created by Yevgeniy Kratko on 16.01.15.
//
//

#import "VOSearchController.h"
#import "NSImageView+WebCache.h"
#import "VOFeedPost.h"
#import "NSColor+Flat.h"

//#import "VKConnector.h"
@import SwiftyVK;

@interface VOSearchController () <NSTextFieldDelegate, NSTableViewDataSource, NSTableViewDelegate>
@property (weak) IBOutlet NSSearchField *searchField;
@property (weak) IBOutlet NSTableView *tableView;
@property (strong, nonatomic) NSMutableArray *datasource;
@property (strong, nonatomic) NSString *stringQuery;
@property (assign, nonatomic) BOOL needAppend;
- (IBAction)goToLikes:(id)sender;
@end

@implementation VOSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self setDatasource:[NSMutableArray array]];
    [self setNeedAppend:YES];
}

- (void)appendDatasource:(NSString*)startFrom
{
    if (self.needAppend && self.stringQuery.length > 0) {
        [self.searchField setEnabled:NO];
        [self setNeedAppend:NO];
        __weak typeof(self) weakSelf = self;
        if (!startFrom) {
            [self.datasource removeAllObjects];
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSDictionary *params = startFrom ? @{@"q":weakSelf.stringQuery, @"start_from":startFrom} : @{@"q":weakSelf.stringQuery};
            NSArray *res = [[[[VKUser currentUser] newsfeed] searchCustomOptions:params] valueForKey:@"response"];
            NSInteger count = [[res firstObject] integerValue];
            res = [res filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF isKindOfClass:%@", [NSDictionary class]]];
            NSArray *extendedRes = [weakSelf feedItemsWithInfo:res];
            [weakSelf.datasource addObjectsFromArray:extendedRes];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tableView reloadData];
                [weakSelf.searchField setEnabled:YES];
            });
        });
    }
}

- (void)controlTextDidEndEditing:(NSNotification *)obj
{
    [self setStringQuery:[obj.object stringValue]];
    [self appendDatasource:nil];
}

- (NSArray*)feedItemsWithInfo:(NSArray*)info
{
    NSMutableArray *userIDs = [[info valueForKey:@"from_id"] mutableCopy];
    NSMutableArray *groupIDs = [[userIDs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF < 0"]] mutableCopy];
    [userIDs removeObjectsInArray:groupIDs];
    
    [groupIDs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [groupIDs replaceObjectAtIndex:idx withObject:[NSString stringWithFormat:@"%ld",ABS([obj integerValue])]];
    }];

    NSError *error = nil;
    NSString *groupStr = [[[NSSet setWithArray:groupIDs] allObjects] componentsJoinedByString:@","];
    NSArray *groupSources = [[[VKConnector sharedInstance] performVKMethod:kVKGroupsGetById options:@{@"group_ids":groupStr} error:&error] valueForKey:@"response"];
    NSString *userStr = [[[NSSet setWithArray:userIDs] allObjects] componentsJoinedByString:@","];
    NSArray *userSources = [[[VKConnector sharedInstance] performVKMethod:kVKUsersGet options:@{@"user_ids":userStr, @"fields":@"photo_200"} error:&error] valueForKey:@"response"];
    NSArray *all = [[NSArray arrayWithArray:groupSources] arrayByAddingObjectsFromArray:userSources];
    NSMutableArray *posts = [NSMutableArray arrayWithCapacity:[all count]];
    [info enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        NSInteger from = [[obj valueForKey:@"from_id"] integerValue];
        NSInteger itemID = [[obj valueForKey:@"id"] integerValue];
        NSPredicate *predicate = from < 0 ? [NSPredicate predicateWithFormat:@"SELF.gid == %d", ABS(from)] : [NSPredicate predicateWithFormat:@"SELF.uid == %d", from];
        NSDictionary *source = [[all filteredArrayUsingPredicate:predicate] firstObject];
        VOFeedPost *post = [[VOFeedPost alloc] initWithAuthorInfo:source];
        [post setPostURL:[NSString stringWithFormat:@"https://vk.com/feed?w=wall%ld_%ld", from, itemID]];
        [post setItemID:itemID];
        NSString *text = [obj valueForKey:@"text"];
        if ([text length] == 0) {
            text = [obj valueForKeyPath:@"attachment.link.description"];
        }
        text = [text length] == 0 ? @"" : [text stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        [post setText:text];
        
        [post setDate:[NSDate dateWithTimeIntervalSince1970:[[obj valueForKey:@"date"] doubleValue]]];
        [post setLikes:[[obj valueForKeyPath:@"likes.count"] integerValue]];
        [posts addObject:post];
    }];
    [posts sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]];
    return posts;
}

#pragma mark - TableView Datasource & Delegate

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [self.datasource count];
}

- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row
{
    return 130;
}

- (NSView*)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    static NSString *const identifier = @"cell";
    NSTableCellView *cell = [tableView makeViewWithIdentifier:identifier owner:self];
    VOFeedPost *item = [self.datasource objectAtIndex:row];
    [cell.textField setStringValue:item.text];
    NSDictionary *info = item.authorInfo;
    NSString *sourceStr = nil;
    if ([info valueForKey:@"gid"]) {
        sourceStr = [info valueForKey:@"name"];
    }
    else if ([info valueForKey:@"uid"]) {
        sourceStr = [NSString stringWithFormat:@"%@ %@", [info valueForKey:@"first_name"], [info valueForKey:@"last_name"]];
    }
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateStyle:NSDateFormatterShortStyle];
    [f setTimeStyle:NSDateFormatterShortStyle];
    [[cell viewWithTag:997] setStringValue:[f stringFromDate:item.date]];
    [[cell viewWithTag:998] setStringValue:sourceStr];
    [cell.imageView setImage:item.avatar];
    [(NSButton*)[cell viewWithTag:999] setAttributedTitle:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"♥ %ld", item.likes] attributes:@{NSForegroundColorAttributeName:[NSColor wetAsphaltColor]}]];
    return cell;
}

- (IBAction)goToLikes:(id)sender
{
    [self performSegueWithIdentifier:@"tolikes" sender:sender];
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender
{
    NSInteger row = [self.tableView rowForView:sender];
    VOFeedPost *post = [self.datasource objectAtIndex:row];
    NSViewController *likesCtrl = [segue destinationController];
    [likesCtrl setValue:post forKey:@"post"];
    
}
@end
