//
//  VOSharingController.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 26.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOSharingController.h"
#import "VKConnector.h"
#import "NSImage+Extra.h"
#import "VKAccessToken.h"
#import "YRKSpinningProgressIndicator.h"
#import "VOView.h"
#import "NSColor+Flat.h"
#import "NSString+Extra.h"

@interface VOGroup : NSObject
@property (assign, nonatomic, readonly) NSInteger groupID;
@property (assign, nonatomic, readonly) BOOL isClosed;
@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSString *screenName;
@property (strong, nonatomic, readonly) NSImage *photo;
- (instancetype)initWithInfo:(NSDictionary*)info;
@end
@implementation VOGroup

- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        _groupID = [info[@"gid"] integerValue];
        _isClosed = [info[@"is_closed"] boolValue];
        _name = info[@"name"];
        _screenName = info[@"screen_name"];
        _photo = [[NSImage alloc] initWithContentsOfURL:[NSURL URLWithString:info[@"photo_big"]]];
    }
    return self;
}

@end

@interface VOSharingController () <NSPopoverDelegate>
- (IBAction)addImage:(id)sender;
- (IBAction)post:(id)sender;
- (IBAction)close:(id)sender;
@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSImageView *imageView;
@property (unsafe_unretained) IBOutlet NSTextView *textView;
@property (strong, nonatomic) NSArray *publics;
@property (strong, nonatomic) NSImage *image;
@end

@implementation VOSharingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self setImage:[NSImage imageNamed:@"addImage"]];
    [(VOView*)self.view setBackgroundColor:[NSColor cloudsColor]];
}

- (void)viewDidAppear
{
    [super viewDidAppear];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf loadPublics];
    });
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf loadLatestPost];
    });
}

- (IBAction)addImage:(id)sender
{
    NSOpenPanel *open = [NSOpenPanel openPanel];
    [open setAllowedFileTypes:@[@"png", @"PNG", @"jpg", @"JPG", @"jpeg", @"JPEG"]];
    [open setCanChooseDirectories:NO];
    [open setAllowsMultipleSelection:NO];
    NSInteger code = [open runModal];
    if (code == NSModalResponseOK) {
        NSURL *url = [open URL];
        NSImage *img = [[NSImage alloc] initWithContentsOfURL:url];
        [self.imageView setImage:img];
        [self setImage:img];
    }
}

- (void)loadPublics
{
    id res = [[[VKUser currentUser] groups] listCustomOptions:@{@"count":@(1000), @"extended":@(YES), @"filter":@"publics"}];
    if (res) {
        NSMutableArray *groups = [NSMutableArray array];
        [res[@"response"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                VOGroup *group = [[VOGroup alloc] initWithInfo:obj];
                [groups addObject:group];
            }
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setPublics:(NSArray*)groups];
        });
    }
}

- (void)loadLatestPost
{
    NSError *error = nil;
    id res = [[VKConnector sharedInstance] performVKMethod:kVKWallGet options:@{@"owner_id":@(-(ABS(self.groupID))), @"count":@(1)} error:&error];
    if (res && !res[@"error"] && !error) {
        NSDictionary *dict = res[@"response"][1];
        __block NSURL *imageURL = nil;
        NSArray *keys = @[@"src_xxbig", @"src_xbig", @"src_big", @"src", @"src_small"];
        [keys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
            NSString *urlStr = [dict valueForKeyPath:[NSString stringWithFormat:@"attachment.photo.%@", key]];
            if (urlStr.length > 0) {
                *stop = YES;
                imageURL = [NSURL URLWithString:urlStr];
            }
        }];
        NSImage *image = [[NSImage alloc] initWithContentsOfURL:imageURL];
        NSString *text = [dict[@"text"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf setImage:image];
            [weakSelf.textView setString:text];
        });
    }
}

- (IBAction)post:(id)sender
{
    NSInteger row = [self.tableView rowForView:sender];
    VOGroup *group = [self.publics objectAtIndex:row];
    [(NSButton*)sender setEnabled:NO];
    YRKSpinningProgressIndicator *progress = [[[[sender superview] subviews] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [YRKSpinningProgressIndicator class]]] firstObject];
    [progress setIndeterminate:YES];
    [progress setHidden:NO];
    [progress startAnimation:self];
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error = nil;
        id res = [[VKConnector sharedInstance] performVKMethod:kVKPhotosGetWallUploadServer options:@{@"group_id":@(group.groupID)} error:&error];
        if (res && !res[@"error"] && !error) {
            NSString *uploadURL = [[res valueForKeyPath:@"response.upload_url"] copy];
            NSURLRequest *postImageRequest = [weakSelf postImageRequest:weakSelf.image URL:uploadURL];
            NSData *responseData = [NSURLConnection sendSynchronousRequest:postImageRequest returningResponse:nil error:nil];
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
            NSString *hash = [dict objectForKey:@"hash"];
            NSString *photo = [dict objectForKey:@"photo"];
            NSString *server = [dict objectForKey:@"server"];
            res = [[VKConnector sharedInstance] performVKMethod:kVKPhotosSaveWallPhoto options:@{@"group_id":@(group.groupID), @"server":server, @"photo":photo, @"hash":hash} error:&error];
            if (res && !res[@"error"] && !error) {
                NSString *photoID = [[[res valueForKey:@"response"] firstObject] valueForKey:@"id"];
                res = [[VKConnector sharedInstance] performVKMethod:kVKWallPost options:@{@"owner_id":@(-group.groupID), @"message":[weakSelf.textView.string stringUsingEncoding:NSUTF8StringEncoding], @"attachment":photoID} error:&error];
                if ([res valueForKeyPath:@"response.post_id"] && !error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [progress stopAnimation:weakSelf];
                        [progress setHidden:YES];
                        [(NSButton*)sender setImage:[NSImage imageNamed:@"didSend"]];
                    });
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [progress stopAnimation:weakSelf];
                        [progress setHidden:YES];
                        [(NSButton*)sender setImage:[NSImage imageNamed:@"notSend"]];
                    });
                }
            }
        }
    });
}

- (IBAction)close:(id)sender
{
    [self dismissController:self];
}

- (NSURLRequest*)postImageRequest:(NSImage*)image URL:(NSString*)url
{
    NSData *imageData = [image JPGRepresentation];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
    CFUUIDRef uuid = CFUUIDCreate(nil);
    NSString *uuidString = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuid));
    CFRelease(uuid);
    NSString *stringBoundary = [NSString stringWithFormat:@"0xKhTmLbOuNdArY-%@",uuidString];
    NSString *endItemBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data;  boundary=%@", stringBoundary];
    
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"photo\"; filename=\"photo.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData];
    [body appendData:[[NSString stringWithFormat:@"%@",endItemBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Добавляем body к NSMutableRequest
    [request setHTTPBody:body];
    return request;
}
@end
