//
//  VOStatController.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 30.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOStatController.h"
#import "VKConnector.h"
#import "NSDate+TimeAgo.h"
#import "VOStatistics.h"

#import "PlotItem.h"
#import "PlotView.h"
#import "VOExtendedVisitorsPlot.h"

@interface VOStatController () <NSTableViewDataSource, NSTableViewDelegate, NSPageControllerDelegate>
@property (weak) IBOutlet NSTableView *tableView;
@property (strong, nonatomic) NSArray *sections;
@property (strong, nonatomic) VOStatistics *statistics;

@property (weak) IBOutlet PlotView *plotView;
@property (strong, nonatomic) PlotItem *plotItem;
@end

@implementation VOStatController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self setSections:@[@"Посетители и просмотры", @"Пол", @"Возраст", @"География", @"Источники", @"Участники"]];
    [self.tableView setEnabled:NO];
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf getStatsForLastYear];
    });
}

- (void)getStatsForLastYear
{
    NSDate *lastYear  = [NSDate dateAgo:Year];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"YYYY-MM-dd"];
    NSError *error = nil;
    id res = [[VKConnector sharedInstance] performVKMethod:kVKStatsGet options:@{@"group_id":@(self.groupID), @"date_from":[f stringFromDate:lastYear]} error:&error];
    if (res && !res[@"error"] && !error) {
        VOStatistics *statistics = [[VOStatistics alloc] initWithInfo:res];
        [self setStatistics:statistics];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
            [self.tableView setEnabled:YES];
        });
    }
}

- (void)setPlotItem:(PlotItem *)plotItem
{
    if (_plotItem != plotItem) {
        [_plotItem killGraph];
        _plotItem = plotItem;
        [_plotItem renderInView:self.plotView withTheme:nil animated:YES];
    }
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [self.sections count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    return [self.sections objectAtIndex:row];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    NSInteger row = [self.tableView selectedRow];
    PlotItem *plot = nil;
    switch (row) {
        case 0: {
            NSMutableArray *array = [NSMutableArray array];
            [self.statistics.days enumerateObjectsUsingBlock:^(VOStatDay *day, NSUInteger idx, BOOL *stop) {
                NSDictionary *item = @{@"day":day.date, @"visitors":@(day.visitors), @"views":@(day.views)};
                [array insertObject:item atIndex:0];
            }];
            plot = [[VOExtendedVisitorsPlot alloc] init];
            [(VOExtendedVisitorsPlot*)plot setDatePoints:array];
            break;
        }
        default:break;
    }
    [self setPlotItem:plot];
}

@end
