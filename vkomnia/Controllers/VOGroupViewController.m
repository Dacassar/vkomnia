//
//  VOGroupViewController.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOGroupViewController.h"
#import "VKGroups.h"
#import "VKConnector.h"

#import "VOViewsVisitorsPlot.h"
#import "PlotView.h"

#import "NSImageView+WebCache.h"
@interface VOGroupViewController ()
@property (weak) IBOutlet NSImageView *groupImage;

@property (weak) IBOutlet NSTextField *groupNameLabel;
@property (weak) IBOutlet NSTextField *groupSNameLabel;
@property (weak) IBOutlet NSTextField *membersCountLabel;

@property (weak) IBOutlet PlotView *plotView;

@property (strong, nonatomic) VOViewsVisitorsPlot *statisticsPlot;

@property (weak, nonatomic) NSString *incoming;
@property (weak, nonatomic) NSString *outgoing;
@property (strong, nonatomic) NSArray *statDataPoints;
@property (weak) IBOutlet NSPopUpButton *inoutSwitch;

- (IBAction)killZombies:(id)sender;
- (IBAction)closeSheet:(id)sender;
- (IBAction)inOutSwitch:(id)sender;
@end

@implementation VOGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [(NSObject*)[[NSApplication sharedApplication] delegate] setValue:nil forKey:@"groupMembers"];
    // Do view setup here.
    [self.groupNameLabel setStringValue:[self.groupInfo valueForKey:@"name"]];
    [self.groupSNameLabel setStringValue:[self.groupInfo valueForKey:@"screen_name"]];
    
    VOViewsVisitorsPlot *plot = [[VOViewsVisitorsPlot alloc] init];
    [plot setAllowInteraction:NO];
    [self setStatisticsPlot:plot];
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        VKGroups *groups = [[VKUser currentUser] groups];
        NSString *gid = [NSString stringWithFormat:@"%@",[weakSelf.groupInfo valueForKey:@"gid"]];
        id response = [[groups byIDs:@[gid] fields:@[@"city",@"country",@"place",@"description",@"members_count",@"counters"]] valueForKey:@"response"][0];
        [weakSelf setGroupInfo:response];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.groupImage setImageURL:[NSURL URLWithString:[response valueForKey:@"photo_medium"]]];
            [weakSelf.membersCountLabel setStringValue:[response valueForKey:@"members_count"]];
            [weakSelf getStatisticsForWeek:gid];
        });
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSError *error = nil;
            id groupID = [self.groupInfo valueForKey:@"gid"];
            NSInteger count = 1;
            NSInteger offset = 0;
            NSMutableArray *allUsers = [NSMutableArray array];
            while (count > offset) {
                NSString *script = [NSString stringWithFormat:@"var groupId=%@;\nvar offset=%ld;\nvar _acl=22;\nvar members=API.groups.getMembers({gid:groupId});\n_acl=_acl-1;\nvar count=members.count;\nvar users=[];\nwhile(_acl>1&&offset<count){\n\tvar _members=API.groups.getMembers({gid:groupId,offset:offset}); _acl=_acl-1;\n\tusers=users+API.users.get({uids:_members.users, fields:\"photo_200\"}); _acl = _acl - 1;\n\toffset=offset+1000;\n}\nvar result = {\n\tcount:count,\n\toffset:offset,\n\tusers:users\n};\nreturn result;", groupID, offset];
                id res = [[VKConnector sharedInstance] performVKMethod:@"execute" options:@{@"code":script} error:&error];
                if (res && !error) {
                    count = [[res valueForKeyPath:@"response.count"] integerValue];
                    offset = [[res valueForKeyPath:@"response.offset"] integerValue];
                    [allUsers addObjectsFromArray:[res valueForKeyPath:@"response.users"]];
                }
                else {
                    break;
                }
            }
            setCurrentGroupMembers((NSArray*)allUsers);
        });
    });
}

- (void)getStatisticsForWeek:(NSString*)groupID
{
    NSDate *lastWeek  = [[NSDate date] dateByAddingTimeInterval: -604800.0];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"YYYY-MM-dd"];
    NSError *error = nil;
    id response = [[[VKConnector sharedInstance] performVKMethod:kVKStatsGet options:@{@"group_id":groupID, @"date_from":[f stringFromDate:lastWeek]} error:&error] valueForKey:@"response"];
    if (response && !error) {
        NSMutableArray *data = [NSMutableArray array];
        [response enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *day = @{@"day":obj[@"day"], @"visitors":obj[@"visitors"], @"views":obj[@"views"], @"in":[obj[@"subscribed"] isKindOfClass:[NSNull class]] ? @(0) : obj[@"subscribed"], @"out":[obj[@"unsubscribed"] isKindOfClass:[NSNull class]] ? @(0) : obj[@"unsubscribed"]};
            [data insertObject:day atIndex:0];
        }];
        [self setStatDataPoints:data];
        [self.statisticsPlot setDataPoints:data];
        [self.statisticsPlot renderInView:self.plotView withTheme:nil animated:YES];
        
        [self inOutSwitch:self.inoutSwitch];
    }
}

- (IBAction)killZombies:(id)sender
{
    NSMutableArray *currentMembers = [currentGroupMembers() mutableCopy];
    [currentMembers filterUsingPredicate:[NSPredicate predicateWithFormat:@"deactivated == %@ OR deactivated == %@", @"deleted", @"banned"]];
    NSInteger count = [currentMembers count];
    if (count > 0) {
        NSAlert *killAlert = [[NSAlert alloc] init];
        [killAlert setMessageText:[NSString stringWithFormat:@"Найдено %ld удаленных или замороженных пользователей", count]];
        [killAlert setInformativeText:@"Вы уверены, что хотите удалить их из группы?"];
        [killAlert setAlertStyle:NSInformationalAlertStyle];
        [killAlert addButtonWithTitle:@"Удалить"];
        [killAlert addButtonWithTitle:@"Закрыть"];
        NSModalResponse response = [killAlert runModal];
        if (response == 1000) {
            NSInteger offset = 0;
            NSArray *ids = [currentMembers valueForKey:@"uid"];
            NSInteger deleted = 0;
            while (offset < count) {
                NSInteger diff = MIN(count-offset, 25);
                NSArray *subarray = [ids subarrayWithRange:NSMakeRange(offset, diff)];
                NSString *subarrayStr = [NSString stringWithFormat:@"[%@]",[subarray componentsJoinedByString:@","]];
                NSString *code = [NSString stringWithFormat:@"var acl=25;\nvar ids=%@;\nvar counter=%ld;\nvar result=0;\n\nwhile (acl > 0 && counter > 0) {\n\tvar usrid=ids[counter];\n\tvar res=API.groups.removeUser({group_id:%@, user_id:usrid});\n\tcounter=counter-1;\n\tacl=acl-1;\n\tif (res==1) {\n\t\tresult=result+1;\n\t}\n}\nreturn result;", subarrayStr, subarray.count, self.groupInfo[@"gid"]];
                NSError *error = nil;
                id res = [[VKConnector sharedInstance] performVKMethod:@"execute" options:@{@"code":code} error:&error];
                deleted += [res[@"response"] integerValue];
                offset += diff;
            }
            NSAlert *deletedAlert = [[NSAlert alloc] init];
            [deletedAlert setMessageText:@"Поздравляем, Ваша группа стала чище!"];
            [deletedAlert setInformativeText:[NSString stringWithFormat:@"Было удалено %ld неактивных пользователей", deleted]];
            [killAlert setAlertStyle:NSInformationalAlertStyle];
            [killAlert runModal];
        }
    }
    else {
        NSAlert *cleanAlert = [[NSAlert alloc] init];
        [cleanAlert setMessageText:@"Поздравляем!"];
        [cleanAlert setInformativeText:@"В Вашей группе нет неактивных пользователей."];
        [cleanAlert runModal];
    }
}

- (IBAction)closeSheet:(id)sender
{
    [self dismissController:self];
}

- (IBAction)inOutSwitch:(NSPopUpButton*)sender
{
    NSInteger tag = sender.selectedTag;
    NSString *incoming = nil;
    NSString *outgoing = nil;
    switch (tag) {
        case 1: {
            id last = [self.statDataPoints lastObject];
            incoming = [NSString stringWithFormat:@"%@ ▲",last[@"in"]];
            outgoing = [NSString stringWithFormat:@"%@ ▼",last[@"out"]];
            break;
        }
        case 7: {
            NSArray *allIn = [self.statDataPoints valueForKey:@"in"];
            __block NSInteger inc = 0;
            [allIn enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                inc += [obj integerValue];
            }];
            
            NSArray *allOut = [self.statDataPoints valueForKey:@"out"];
            __block NSInteger outg = 0;
            [allOut enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                outg += [obj integerValue];
            }];
            
            incoming = [NSString stringWithFormat:@"%ld ▲", inc];
            outgoing = [NSString stringWithFormat:@"%ld ▼", outg];
            break;
        }
        default:break;
    }
    [self setIncoming:incoming];
    [self setOutgoing:outgoing];
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"share"] || [segue.identifier isEqualToString:@"stats"]) {
        [segue.destinationController setValue:[self.groupInfo valueForKey:@"gid"] forKey:@"groupID"];
    }
}

@end
