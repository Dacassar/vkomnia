//
//  VOMessageController.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 21.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class VOLikedItem;

extern NSString *const VOMessageDidSentNotification;
extern NSString *const VOMessageDidCloseNotification;

@interface VOMessageController : NSViewController
@property (strong, nonatomic) VOLikedItem *like;
@property (strong, nonatomic) NSString *postURL;
@end
