//
//  VOLikesController.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 19.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOLikesController.h"
#import "VKConnector.h"
#import "VOFeedPost.h"
#import "VOLikedItem.h"
#import "VOLikeCell.h"
#import "VOCollectionSegue.h"
#import "VOMessageController.h"
#import "VOView.h"
#import "NSColor+Flat.h"

typedef NS_OPTIONS(NSInteger, VOLikesFilter) {
    VOLikesNoFilter = 0,
    VOLikesInvitedFilter = (0x1 << 1),
    VOLikesMemberFilter = (0x1 << 2)
};

@interface VOLikesController () <NSCollectionViewDelegate>
@property (weak) IBOutlet NSCollectionView *collectionView;
@property (weak) IBOutlet NSTextField *availableLabel;
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;
@property (strong, nonatomic) NSMutableArray *likes;
@property (assign, nonatomic) BOOL filterInvited;
@property (assign, nonatomic) BOOL filterMembers;

@property (assign, nonatomic) NSInteger prMax;
@property (assign, nonatomic) NSInteger prCur;

@property (strong, nonatomic) NSArray *filterBuffer;
@property (weak, nonatomic) id clickObserver;
@property (weak, nonatomic) id sendObserver;
@property (weak, nonatomic) id closeObserver;

- (IBAction)close:(id)sender;
- (IBAction)switchMembersFilter:(id)sender;
- (IBAction)switchInvitedFilter:(id)sender;
@end

@implementation VOLikesController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self.collectionView setItemPrototype:[[NSStoryboard storyboardWithName:@"Main" bundle:nil] instantiateControllerWithIdentifier:@"collectionItem"]];
    
    [self setLikes:[NSMutableArray array]];
    [self setFilterMembers:YES];
    [self setFilterInvited:YES];
    [(VOView*)self.view setBackgroundColor:[NSColor cloudsColor]];
}

- (void)viewDidAppear
{
    [super viewDidAppear];
    
    __weak typeof(self) weakSelf = self;
    self.clickObserver = [[NSNotificationCenter defaultCenter] addObserverForName:VOLikePressedNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        if (note.object) {
            VOLikedItem *item = note.object;
            [weakSelf performSegueWithIdentifier:@"message" sender:item];
        }
    }];
    
    self.sendObserver = [[NSNotificationCenter defaultCenter] addObserverForName:VOMessageDidSentNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        VOLikedItem *item = note.object;
        NSMutableArray *invited = [invitedUsers() mutableCopy];
        [invited addObject:@(item.itemID)];
        setInvitedUsers((NSArray*)invited);
        [weakSelf willChangeValueForKey:@"likes"];
        [weakSelf.likes removeObject:item];
        [weakSelf didChangeValueForKey:@"likes"];
        [weakSelf.availableLabel setStringValue:[NSString stringWithFormat:@"Available: %ld", [weakSelf.likes count]]];
    }];
    
    self.closeObserver = [[NSNotificationCenter defaultCenter] addObserverForName:VOMessageDidCloseNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [weakSelf.collectionView setSelectionIndexes:nil];
    }];
    
    [self.likes removeAllObjects];
    
    [self.progressIndicator setIndeterminate:YES];
    [self.progressIndicator setHidden:NO];
    [self setPrCur:0];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSNumber *ownerID = @(-[[weakSelf.post.authorInfo valueForKey:@"gid"] integerValue]);
        if (!ownerID) {
            ownerID = [self.post.authorInfo valueForKey:@"uid"];
        }
        NSError *error = nil;
        NSString *code = [NSString stringWithFormat:@"var likes = API.likes.getList({type:\"post\", owner_id:%@, item_id:%@, extended:1, count:1000});\nvar ids = likes.items@.id;\nvar profiles = API.users.get({user_ids:ids, fields:\"photo_200,bdate,can_write_private_message,last_seen,country\"});\nreturn profiles;", ownerID, @(weakSelf.post.itemID)];
        id res = [[VKConnector sharedInstance] performVKMethod:@"execute" options:@{@"code":code, @"v":@(5.27)} error:&error];
        if (res && ![res valueForKey:@"error"] && !error) {
            NSArray *items = [weakSelf itemsFromResponse:res];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.progressIndicator setHidden:YES];
                [weakSelf willChangeValueForKey:@"likes"];
                [weakSelf.likes addObjectsFromArray:items];
                [weakSelf didChangeValueForKey:@"likes"];
                [weakSelf applyLikesFilter:[weakSelf likesFilter]];
            });
        }
    });
}

- (NSArray*)itemsFromResponse:(NSDictionary*)response
{
    [self.progressIndicator setIndeterminate:NO];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.can_write_private_message == %@", @(YES)];
    NSArray *dicts = [(NSArray*)[response valueForKey:@"response"] filteredArrayUsingPredicate:predicate];
    [self setPrMax:[dicts count]];
    __weak typeof(self) weakSelf = self;
    NSMutableArray *users = [NSMutableArray array];
    [dicts enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        [weakSelf setPrCur:idx];
        VOLikedItem *user = [[VOLikedItem alloc] initWithInfo:obj];
        [users addObject:user];
    }];
    return (NSArray*)users;
}

- (void)applyLikesFilter:(VOLikesFilter)filter
{
    [self willChangeValueForKey:@"likes"];
    
    if (filter == VOLikesNoFilter) {
        if (self.filterBuffer) {
            [self setLikes:[self.filterBuffer mutableCopy]];
            [self setFilterBuffer:nil];
        }
    }
    else if (filter == VOLikesMemberFilter) {
        if (!self.filterBuffer) {
            [self setFilterBuffer:[self.likes copy]];
        }
        else {
            [self setLikes:[self.filterBuffer mutableCopy]];
        }
        NSArray *currentMembersIDs = [currentGroupMembers() valueForKey:@"uid"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF.itemID IN %@)", currentMembersIDs];
        [self.likes filterUsingPredicate:predicate];
    }
    else if (filter == VOLikesInvitedFilter) {
        if (!self.filterBuffer) {
            [self setFilterBuffer:[self.likes copy]];
        }
        else {
            [self setLikes:[self.filterBuffer mutableCopy]];
        }
        NSArray *invited = invitedUsers();
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (SELF.itemID IN %@)", invited];
        [self.likes filterUsingPredicate:predicate];
    }
    else if (filter == (VOLikesInvitedFilter | VOLikesMemberFilter)) {
        if (!self.filterBuffer) {
            [self setFilterBuffer:[self.likes copy]];
        }
        else {
            [self setLikes:[self.filterBuffer mutableCopy]];
        }
        NSArray *currentMembersIDs = [currentGroupMembers() valueForKey:@"uid"];
        NSArray *invited = invitedUsers();
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(NOT (SELF.itemID IN %@)) AND (NOT (SELF.itemID IN %@))", currentMembersIDs, invited];
        [self.likes filterUsingPredicate:predicate];
    }
    [self.availableLabel setStringValue:[NSString stringWithFormat:@"Available: %ld", [self.likes count]]];
    
    [self didChangeValueForKey:@"likes"];
}

- (VOLikesFilter)likesFilter
{
    if (self.filterInvited && !self.filterMembers) {
        return VOLikesInvitedFilter;
    }
    else if (!self.filterInvited && self.filterMembers) {
        return VOLikesMemberFilter;
    }
    else if (self.filterInvited && self.filterMembers) {
        return VOLikesInvitedFilter|VOLikesMemberFilter;
    }
    return VOLikesNoFilter;
}

- (IBAction)close:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver:self.clickObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self.sendObserver];
    [self setClickObserver:nil];
    [self setSendObserver:nil];
    
    [self dismissController:self];
}

- (IBAction)switchMembersFilter:(id)sender
{
    [self  setFilterMembers:[sender state] == NSOnState];
    [self applyLikesFilter:[self likesFilter]];
}

- (IBAction)switchInvitedFilter:(id)sender
{
    [self setFilterInvited:[sender state] == NSOnState];
    [self applyLikesFilter:[self likesFilter]];
}

- (void)prepareForSegue:(VOCollectionSegue *)segue sender:(id)sender
{
    NSInteger index = [self.likes indexOfObject:sender];
    NSRect rect = [self.collectionView frameForItemAtIndex:index];
    [segue setRelativeRect:rect];
    [segue.destinationController setLike:sender];
    [segue.destinationController setPostURL:self.post.postURL];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self.clickObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self.sendObserver];
}

@end
