//
//  VOSharingController.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 26.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VOSharingController : NSViewController
@property (assign, nonatomic) NSInteger groupID;
@end
