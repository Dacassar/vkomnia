//
//  AppDelegate.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 23.12.14.
//  Copyright (c) 2014 Yevgeniy Kratko. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
