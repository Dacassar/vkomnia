//
//  VOStatItem.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 02.02.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOStatAtom : NSObject
- (instancetype)initWithInfo:(NSDictionary*)info;
@end

@interface VOStatistics : VOStatAtom
@property (strong, nonatomic, readonly) NSArray *days;
@end

@interface VOStatAge : VOStatAtom
@property (assign, nonatomic, readonly) NSInteger age_12_18;
@property (assign, nonatomic, readonly) NSInteger age_18_21;
@property (assign, nonatomic, readonly) NSInteger age_21_24;
@property (assign, nonatomic, readonly) NSInteger age_24_27;
@property (assign, nonatomic, readonly) NSInteger age_27_30;
@property (assign, nonatomic, readonly) NSInteger age_30_35;
@property (assign, nonatomic, readonly) NSInteger age_35_45;
@property (assign, nonatomic, readonly) NSInteger age_45_100;
@end

@interface VOStatCity : VOStatAtom
@property (strong, nonatomic, readonly) NSString *name;
@property (assign, nonatomic, readonly) NSInteger code;
@property (assign, nonatomic, readonly) NSInteger visitors;
@end

@interface VOStatCountry : VOStatAtom
@property (strong, nonatomic, readonly) NSString *code;
@property (assign, nonatomic, readonly) NSInteger codeN;
@property (assign, nonatomic, readonly) NSInteger visitors;
@end

@interface VOStatSex : VOStatAtom
@property (assign, nonatomic, readonly) NSInteger female;
@property (assign, nonatomic, readonly) NSInteger male;
@end

@interface VOStatDay : VOStatAtom
@property (strong, nonatomic, readonly) NSDate *date;
@property (assign, nonatomic, readonly) NSInteger views;
@property (assign, nonatomic, readonly) NSInteger visitors;
@property (strong, nonatomic, readonly) VOStatAge *age;
@property (strong, nonatomic, readonly) NSArray *cities;
@property (strong, nonatomic, readonly) NSArray *countries;
@property (strong, nonatomic, readonly) VOStatSex *sex;
@end
