//
//  VOCollectionSegue.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 21.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VOCollectionSegue : NSStoryboardSegue
@property (assign, nonatomic) NSRect relativeRect;
@end
