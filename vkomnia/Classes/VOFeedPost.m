//
//  VOFeedPost.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 16.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOFeedPost.h"
#import "VKConnector.h"

@implementation VOFeedPost

- (instancetype)initWithAuthorInfo:(NSDictionary *)authorInfo
{
    self = [super init];
    if (self) {
        _authorInfo = authorInfo;
        NSString *str = [authorInfo valueForKey:@"photo_medium"];
        if ([str length] == 0) {
            str = [authorInfo valueForKey:@"photo_200"];
        }
        NSURL *photoURL = [NSURL URLWithString:str];
        NSImage *img = [[NSImage alloc] initWithContentsOfURL:photoURL];
        _avatar = img;
    }
    return self;
}

@end
