//
//  VOExtendedVisitorsPlot.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 30.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "PlotItem.h"

@interface VOExtendedVisitorsPlot : PlotItem<CPTPlotAreaDelegate, CPTPlotSpaceDelegate, CPTPlotDataSource, CPTScatterPlotDelegate>
@property (strong, nonatomic) NSArray *datePoints;
@property (assign, nonatomic) BOOL byMonth;
@end
