//
//  CurvedScatterPlot.h
//  Plot_Gallery_iOS
//
//  Created by Nino Ag on 23/10/11.

#import "PlotItem.h"

@interface VOViewsVisitorsPlot : PlotItem <CPTPlotAreaDelegate,
                                        CPTPlotSpaceDelegate,
                                        CPTPlotDataSource,
                                        CPTScatterPlotDelegate>
@property (strong, nonatomic) NSArray *dataPoints;
@property (assign, nonatomic) BOOL needTitle;
@property (assign, nonatomic) BOOL allowInteraction;
@end
