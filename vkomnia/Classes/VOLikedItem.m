//
//  VOLikedItem.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 20.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOLikedItem.h"

@implementation VOLikedItem

- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        _name = [NSString stringWithFormat:@"%@ %@", [info valueForKey:@"first_name"], [info valueForKey:@"last_name"]];
        _itemID = [[info valueForKey:@"id"] integerValue];
        _type = VOItemProfile;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterLongStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
        [self setBdateString:[info valueForKey:@"bdate"]];
        [self setLastSeen:[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[info valueForKeyPath:@"last_seen.time"] doubleValue]]]];
        NSImage *image = [[NSImage alloc] initWithContentsOfURL:[NSURL URLWithString:[info valueForKey:@"photo_200"]]];
        if (image) {
            [self setAvatar:image];
        }
        
        NSImage *flag = [NSImage imageNamed:[NSString stringWithFormat:@"%@",[info valueForKeyPath:@"country.id"]]];
        _country = flag;
    }
    return self;
}

@end
