//
//  VOExtendedVisitorsPlot.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 30.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOExtendedVisitorsPlot.h"
#import "NSColor+Flat.h"

static NSString *const kViews   = @"Views";
static NSString *const kVisitors  = @"Visitors";

@interface VOExtendedVisitorsPlot ()
@property (assign, nonatomic) CGFloat currentY;
@end

@implementation VOExtendedVisitorsPlot

-(void)renderInGraphHostingView:(CPTGraphHostingView *)hostingView withTheme:(CPTTheme *)theme animated:(BOOL)animated
{
    /*CGRect bounds = NSRectToCGRect(hostingView.bounds);
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:bounds];
    [self addGraph:graph toHostingView:hostingView];
    [self applyTheme:(CPTTheme*)[NSNull null] toGraph:graph withDefault:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    
    // Setup scatter plot space
    CPTXYPlotSpace *space = (CPTXYPlotSpace*)graph.defaultPlotSpace;
    [space setAllowsUserInteraction:YES];
    [space setDelegate:self];
    
//    // Grid line styles
//    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
//    majorGridLineStyle.lineWidth = 0.75;
//    majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:CPTFloat(0.2)] colorWithAlphaComponent:CPTFloat(0.75)];
//    
//    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
//    minorGridLineStyle.lineWidth = 0.25;
//    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:CPTFloat(0.1)];
    
    CGFloat atom = self.byMonth ? 60*60*24*30 : 60*60*24;
    
    // Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.majorIntervalLength = CPTDecimalFromDouble(atom);
    x.orthogonalCoordinateDecimal = CPTDecimalFromDouble(2.0);
    x.minorTicksPerInterval = 0;
    
    NSInteger maxViews = [[self.datePoints valueForKeyPath:@"@max.views"] integerValue];
    NSInteger maxVisitors = [[self.datePoints valueForKeyPath:@"@max.visitors"] integerValue];
    NSInteger maxY = MAX(maxViews, maxVisitors);
    NSInteger majorInterval = (NSInteger)((CGFloat)maxY/10) + 1;
    CPTXYAxis *y = axisSet.yAxis;
    y.majorIntervalLength = CPTDecimalFromInteger(majorInterval);
    y.minorTicksPerInterval = 5;
    y.orthogonalCoordinateDecimal = CPTDecimalFromDouble(atom);
    
    graph.axisSet.axes = @[x,y];
    
    // Views Plot
    CPTScatterPlot *viewsPlot = [[CPTScatterPlot alloc] init];
    viewsPlot.identifier = kViews;
    viewsPlot.interpolation = CPTScatterPlotInterpolationCurved;
    CPTMutableLineStyle *viewsStyle = [viewsPlot.dataLineStyle mutableCopy];
    viewsStyle.lineWidth = 3;
    viewsStyle.lineColor = [CPTColor redColor];
    viewsPlot.dataLineStyle = viewsStyle;
    viewsPlot.dataSource = self;
    [graph addPlot:viewsPlot];
    
    [space scaleToFitPlots:[graph allPlots]];*/
    /*
    NSDate *refDate = [[self.datePoints firstObject] valueForKey:@"day"];
    NSTimeInterval interval = self.byMonth ? 24*60*60*30 : 24*60*60;
    
    // Setup scatter plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    NSMutableArray *values = [[self.datePoints valueForKey:@"views"] mutableCopy];
    [values addObjectsFromArray:[self.datePoints valueForKey:@"visitors"]];
    [values sortUsingSelector:@selector(compare:)];
    NSInteger minY = [[values firstObject] integerValue];
    NSInteger maxY = [[values lastObject] integerValue];
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(-interval/2) length:CPTDecimalFromDouble(interval * [self.datePoints count] + interval/2)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(minY-12) length:CPTDecimalFromDouble(maxY+15)];
    
    // Axes
    
    // Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 0.75;
    majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:CPTFloat(0.6)] colorWithAlphaComponent:CPTFloat(0.75)];
    
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 0.25;
    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:CPTFloat(0.3)];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x          = axisSet.xAxis;
    x.majorIntervalLength         = CPTDecimalFromDouble(interval);
    x.orthogonalCoordinateDecimal = CPTDecimalFromDouble(minY);
    x.minorTicksPerInterval       = 0;
    x.majorGridLineStyle = majorGridLineStyle;
    x.minorGridLineStyle = minorGridLineStyle;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = kCFDateFormatterShortStyle;
    CPTTimeFormatter *timeFormatter = [[CPTTimeFormatter alloc] initWithDateFormatter:dateFormatter];
    timeFormatter.referenceDate = refDate;
    x.labelFormatter            = timeFormatter;
    //    x.labelRotation             = CPTFloat(M_PI_4);
    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorGridLineStyle = majorGridLineStyle;
    y.minorGridLineStyle = minorGridLineStyle;
    y.majorIntervalLength         = CPTDecimalFromDouble(10);
    y.minorTicksPerInterval       = 5;
    y.orthogonalCoordinateDecimal = CPTDecimalFromDouble(interval);
    
    // Create a plot that uses the data source method
    CPTScatterPlot *viewsPlot = [[CPTScatterPlot alloc] init];
    viewsPlot.identifier = kViews;
    // Make the data source line use curved interpolation
    CPTMutableLineStyle *lineStyle = [viewsPlot.dataLineStyle mutableCopy];
    lineStyle.lineWidth              = 3.0;
    NSColor *viewsColor = [NSColor peterRiverColor];
    lineStyle.lineColor              = [CPTColor colorWithComponentRed:viewsColor.redComponent green:viewsColor.greenComponent blue:viewsColor.blueComponent alpha:1];
    viewsPlot.dataLineStyle = lineStyle;
    viewsPlot.dataSource = self;
    viewsPlot.interpolation = CPTScatterPlotInterpolationCurved;
    
    CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
    symbolLineStyle.lineColor = [[CPTColor blackColor] colorWithAlphaComponent:0.5];
    CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.fill               = [CPTFill fillWithColor:[lineStyle.lineColor colorWithAlphaComponent:0.5]];
    plotSymbol.lineStyle          = symbolLineStyle;
    plotSymbol.size               = CGSizeMake(10.0, 10.0);
    
    viewsPlot.plotSymbol = plotSymbol;
    [graph addPlot:viewsPlot];
    
    CPTScatterPlot *visitorsPlot = [[CPTScatterPlot alloc] init];
    visitorsPlot.identifier = kVisitors;
    CPTMutableLineStyle *visitorsLine = [visitorsPlot.dataLineStyle mutableCopy];
    visitorsLine.lineWidth = 3;
    NSColor *visitorsColor = [NSColor amethystColor];
    visitorsLine.lineColor = [CPTColor colorWithComponentRed:visitorsColor.redComponent green:visitorsColor.greenComponent blue:visitorsColor.blueComponent alpha:1];
    visitorsPlot.dataLineStyle = visitorsLine;
    visitorsPlot.dataSource = self;
    visitorsPlot.interpolation = CPTScatterPlotInterpolationCurved;
    CPTPlotSymbol *visitorSymbol = [plotSymbol copy];
    visitorSymbol.fill = [CPTFill fillWithColor:[visitorsLine.lineColor colorWithAlphaComponent:.5]];
    visitorsPlot.plotSymbol = visitorSymbol;
    
    [graph addPlot:visitorsPlot];
    
    CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
    CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
    
    [xRange expandRangeByFactor:CPTDecimalFromDouble(1.025)];
    xRange.location = plotSpace.xRange.location;
    [yRange expandRangeByFactor:CPTDecimalFromDouble(1.05)];
    x.visibleAxisRange = xRange;
    y.visibleAxisRange = yRange;
    
    [xRange expandRangeByFactor:CPTDecimalFromDouble(1)];
    [yRange expandRangeByFactor:CPTDecimalFromDouble(1)];
    plotSpace.globalXRange = xRange;
    plotSpace.globalYRange = yRange;
    
    // Add legend
    graph.legend                 = [CPTLegend legendWithGraph:graph];
    graph.legend.numberOfRows    = 1;
    graph.legend.textStyle       = x.titleTextStyle;
    graph.legendAnchor           = CPTRectAnchorBottom;
     */
}

#pragma mark - Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return [self.datePoints count];
}

-(id)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num = nil;
    if (fieldEnum == CPTScatterPlotFieldX) {
        NSTimeInterval oneDay = 24 * 60 * 60;
        num = @(oneDay*index);
    }
    else {
        NSString *identifier = (NSString*)plot.identifier;
        num = self.datePoints[index][[identifier lowercaseString]];
    }
    return num;
}

#pragma mark - Plot Space Delegate

- (CGPoint)plotSpace:(CPTPlotSpace *)space willDisplaceBy:(CGPoint)proposedDisplacementVector
{
    NSLog(@"%@", NSStringFromPoint(NSPointFromCGPoint(proposedDisplacementVector)));
    return CGPointMake(0, 10*proposedDisplacementVector.x);
}
//- (CPTPlotRange *)plotSpace:(CPTPlotSpace *)space willChangePlotRangeTo:(CPTPlotRange *)newRange forCoordinate:(CPTCoordinate)coordinate
//{
//    switch (coordinate) {
//        case CPTCoordinateY: {
//            CGFloat diff = self.currentY - newRange.locationDouble;
//            self.currentY += diff;
//
//            CPTXYAxis *xAxis = [space.graph.axisSet.axes firstObject];
//            CPTMutablePlotRange *updatedRange = [xAxis.visibleAxisRange mutableCopy];
//            [updatedRange setLocation:CPTDecimalFromCGFloat(self.currentY)];
//            [xAxis setVisibleAxisRange:updatedRange];
////            updatedRange = space.graph.axisSet.axes;
//        }
//        default:break;
//    }
//    return newRange;
//}

@end
