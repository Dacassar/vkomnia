//
//  VOStatItem.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 02.02.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOStatistics.h"

@implementation VOStatAtom
- (instancetype)initWithInfo:(NSDictionary *)info
{
    return [super init];
}
@end

@implementation VOStatistics
- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        NSArray *daysInfo = [info valueForKey:@"response"];
        NSMutableArray *days = [NSMutableArray array];
        [daysInfo enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            VOStatDay *day = [[VOStatDay alloc] initWithInfo:obj];
            [days addObject:day];
        }];
        _days = (NSArray*)days;
    }
    return self;
}
@end

@implementation VOStatAge
- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        NSArray *ages = [info valueForKey:@"age"];
        __weak typeof(self) weakSelf = self;
        [ages enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            NSString *key = [[obj valueForKey:@"value"] stringByReplacingOccurrencesOfString:@"-" withString:@"_"];
            key = [NSString stringWithFormat:@"age_%@", key];
            id value = [obj valueForKey:@"visitors"];
            [weakSelf setValue:value forKey:key];
        }];
    }
    return self;
}
@end

@implementation VOStatCity
- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        _name = info[@"name"];
        _code = [info[@"value"] integerValue];
        _visitors = [info[@"visitors"] integerValue];
    }
    return self;
}
@end

@implementation VOStatCountry
- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        _code = info[@"code"];
        _codeN = [info[@"value"] integerValue];
        _visitors = [info[@"visitors"] integerValue];
    }
    return self;
}
@end

@implementation VOStatSex
- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        __weak typeof(self) weakSelf = self;
        [info[@"sex"] enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            NSString *key = obj[@"value"];
            key = [key isEqualToString:@"f"] ? @"female" : @"male";
            id value = obj[@"visitors"];
            [weakSelf setValue:value forKey:key];
        }];
    }
    return self;
}
@end

@implementation VOStatDay

- (instancetype)initWithInfo:(NSDictionary *)info
{
    self = [super init];
    if (self) {
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"YYYY-MM-dd"];
        _date = [f dateFromString:[info valueForKey:@"day"]];
        
        _views = [info[@"views"] integerValue];
        _visitors = [info[@"visitors"] integerValue];
        
        NSArray *ageInfo = [info valueForKey:@"age"];
        VOStatAge *age = [[VOStatAge alloc] initWithInfo:@{@"age":ageInfo}];
        _age = age;
        
        NSArray *citiesInfo = [info valueForKey:@"cities"];
        NSMutableArray *cities = [NSMutableArray array];
        [citiesInfo enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            VOStatCity *city = [[VOStatCity alloc] initWithInfo:obj];
            [cities addObject:city];
        }];
        _cities = cities;
        
        NSArray *countriesInfo = [info valueForKey:@"countries"];
        NSMutableArray *countries = [NSMutableArray array];
        [countriesInfo enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            VOStatCountry *city = [[VOStatCountry alloc] initWithInfo:obj];
            [countries addObject:city];
        }];
        _countries = countries;
        
        NSArray *sexInfo = [info valueForKey:@"sex"];
        VOStatSex *sex = [[VOStatSex alloc] initWithInfo:@{@"sex":sexInfo}];
        _sex = sex;
    }
    return self;
}

@end