//
//  VOLikedItem.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 20.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, VOItemType) {
    VOItemProfile,
    VOItemGroup
};

@interface VOLikedItem : NSObject
@property (strong, nonatomic, readonly) NSString *name;
@property (assign, nonatomic, readonly) NSInteger itemID;
@property (strong, nonatomic) NSImage *avatar;
@property (assign, nonatomic, readonly) VOItemType type;
@property (strong, nonatomic) NSString *bdateString;
@property (strong, nonatomic) NSString *lastSeen;
@property (strong, nonatomic, readonly) NSImage *country;
- (instancetype)initWithInfo:(NSDictionary*)info;
@end
