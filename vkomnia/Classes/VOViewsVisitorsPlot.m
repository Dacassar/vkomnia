//
//  CurvedScatterPlot.m
//  Plot_Gallery_iOS
//
//  Created by Nino Ag on 23/10/11.

#import "VOViewsVisitorsPlot.h"
#import "NSColor+Flat.h"

static NSString *const kViews   = @"Views";
static NSString *const kVisitors  = @"Visitors";

@implementation VOViewsVisitorsPlot

-(id)init
{
    if ( (self = [super init]) ) {
        self.section = kLinePlots;
    }

    return self;
}

-(void)renderInGraphHostingView:(CPTGraphHostingView *)hostingView withTheme:(CPTTheme *)theme animated:(BOOL)animated
{
    self.title = self.needTitle ? @"Посетители и просмотры за 7 дней" : nil;
    
    /*CGRect bounds = NSRectToCGRect(hostingView.bounds);
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:bounds];
    [self addGraph:graph toHostingView:hostingView];
    [self applyTheme:(CPTTheme*)[NSNull null] toGraph:graph withDefault:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    
    id refDate = [[self.dataPoints firstObject] valueForKey:@"day"];
    if ([refDate isKindOfClass:[NSString class]]) {
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"YYYY-MM-dd"];
        refDate = [f dateFromString:refDate];
    }
    NSTimeInterval oneDay = 24 * 60 * 60;
    
//    // Setup scatter plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    if (self.allowInteraction) {
        plotSpace.allowsUserInteraction = YES;
        plotSpace.delegate              = self;
        
        
    }
    else {
        NSMutableArray *values = [[self.dataPoints valueForKey:@"views"] mutableCopy];
        [values addObjectsFromArray:[self.dataPoints valueForKey:@"visitors"]];
        [values sortUsingSelector:@selector(compare:)];
        NSInteger minY = [[values firstObject] integerValue];
        NSInteger maxY = [[values lastObject] integerValue];
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(-oneDay/2) length:CPTDecimalFromDouble(oneDay * [self.dataPoints count] + oneDay/2)];
        plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(minY-12) length:CPTDecimalFromDouble(maxY+15)];
        
        // Axes
        // Grid line styles
        CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
        majorGridLineStyle.lineWidth = 0.75;
        majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:CPTFloat(0.6)] colorWithAlphaComponent:CPTFloat(0.75)];
        
        CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
        minorGridLineStyle.lineWidth = 0.25;
        minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:CPTFloat(0.3)];
        
        CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
        CPTXYAxis *x          = axisSet.xAxis;
        x.majorIntervalLength         = CPTDecimalFromDouble(oneDay);
        x.orthogonalCoordinateDecimal = CPTDecimalFromDouble(minY);
        x.minorTicksPerInterval       = 0;
        x.majorGridLineStyle = majorGridLineStyle;
        x.minorGridLineStyle = minorGridLineStyle;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = kCFDateFormatterShortStyle;
        CPTTimeFormatter *timeFormatter = [[CPTTimeFormatter alloc] initWithDateFormatter:dateFormatter];
        timeFormatter.referenceDate = refDate;
        x.labelFormatter            = timeFormatter;
        //    x.labelRotation             = CPTFloat(M_PI_4);
        
        CPTXYAxis *y = axisSet.yAxis;
        y.majorGridLineStyle = majorGridLineStyle;
        y.minorGridLineStyle = minorGridLineStyle;
        y.majorIntervalLength         = CPTDecimalFromDouble(10);
        y.minorTicksPerInterval       = 5;
        y.orthogonalCoordinateDecimal = CPTDecimalFromDouble(oneDay);
        
        // Create a plot that uses the data source method
        CPTScatterPlot *viewsPlot = [[CPTScatterPlot alloc] init];
        viewsPlot.identifier = kViews;
        // Make the data source line use curved interpolation
        CPTMutableLineStyle *lineStyle = [viewsPlot.dataLineStyle mutableCopy];
        lineStyle.lineWidth              = 3.0;
        NSColor *viewsColor = [NSColor peterRiverColor];
        lineStyle.lineColor              = [CPTColor colorWithComponentRed:viewsColor.redComponent green:viewsColor.greenComponent blue:viewsColor.blueComponent alpha:1];
        viewsPlot.dataLineStyle = lineStyle;
        viewsPlot.dataSource = self;
        viewsPlot.interpolation = CPTScatterPlotInterpolationCurved;
        
        CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
        symbolLineStyle.lineColor = [[CPTColor blackColor] colorWithAlphaComponent:0.5];
        CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
        plotSymbol.fill               = [CPTFill fillWithColor:[lineStyle.lineColor colorWithAlphaComponent:0.5]];
        plotSymbol.lineStyle          = symbolLineStyle;
        plotSymbol.size               = CGSizeMake(10.0, 10.0);
        
        viewsPlot.plotSymbol = plotSymbol;
        [graph addPlot:viewsPlot];
        
        CPTScatterPlot *visitorsPlot = [[CPTScatterPlot alloc] init];
        visitorsPlot.identifier = kVisitors;
        CPTMutableLineStyle *visitorsLine = [visitorsPlot.dataLineStyle mutableCopy];
        visitorsLine.lineWidth = 3;
        NSColor *visitorsColor = [NSColor amethystColor];
        visitorsLine.lineColor = [CPTColor colorWithComponentRed:visitorsColor.redComponent green:visitorsColor.greenComponent blue:visitorsColor.blueComponent alpha:1];
        visitorsPlot.dataLineStyle = visitorsLine;
        visitorsPlot.dataSource = self;
        visitorsPlot.interpolation = CPTScatterPlotInterpolationCurved;
        CPTPlotSymbol *visitorSymbol = [plotSymbol copy];
        visitorSymbol.fill = [CPTFill fillWithColor:[visitorsLine.lineColor colorWithAlphaComponent:.5]];
        visitorsPlot.plotSymbol = visitorSymbol;
        
        [graph addPlot:visitorsPlot];
        
        CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
        CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
        
        [xRange expandRangeByFactor:CPTDecimalFromDouble(1.025)];
        xRange.location = plotSpace.xRange.location;
        [yRange expandRangeByFactor:CPTDecimalFromDouble(1.05)];
        x.visibleAxisRange = xRange;
        y.visibleAxisRange = yRange;
        
        [xRange expandRangeByFactor:CPTDecimalFromDouble(1)];
        [yRange expandRangeByFactor:CPTDecimalFromDouble(1)];
        plotSpace.globalXRange = xRange;
        plotSpace.globalYRange = yRange;
        
        // Add legend
        graph.legend                 = [CPTLegend legendWithGraph:graph];
        graph.legend.numberOfRows    = 1;
        graph.legend.textStyle       = x.titleTextStyle;
        graph.legendAnchor           = CPTRectAnchorBottom;
    }*/
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return [self.dataPoints count];
}

-(id)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num = nil;
    if (fieldEnum == CPTScatterPlotFieldX) {
        NSTimeInterval oneDay = 24 * 60 * 60;
        num = @(oneDay*index);
    }
    else {
        NSString *identifier = (NSString*)plot.identifier;
        num = self.dataPoints[index][[identifier lowercaseString]];
    }
    return num;
}

#pragma mark - Plot Space Delegate Methods

- (CPTPlotRange *)plotSpace:(CPTPlotSpace *)space willChangePlotRangeTo:(CPTPlotRange *)newRange forCoordinate:(CPTCoordinate)coordinate
{
//    CPTGraph *theGraph    = space.graph;
//    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)theGraph.axisSet;
//    
//    CPTMutablePlotRange *changedRange = [newRange mutableCopy];
//    
//    switch ( coordinate ) {
//        case CPTCoordinateX:
//            [changedRange expandRangeByFactor:CPTDecimalFromDouble(1.025)];
//            changedRange.location          = newRange.location;
//            axisSet.xAxis.visibleAxisRange = changedRange;
//            break;
//            
//        case CPTCoordinateY:
//            [changedRange expandRangeByFactor:CPTDecimalFromDouble(1.05)];
//            axisSet.yAxis.visibleAxisRange = changedRange;
//            break;
//            
//        default:
//            break;
//    }
    
    return newRange;
}

@end
