//
//  VOCollectionSegue.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 21.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOCollectionSegue.h"

@implementation VOCollectionSegue

- (void)perform
{
    NSView *view = [self.identifier isEqualToString:@"tolikes"] ? [[self sourceController] valueForKey:@"tableView"] : [[self sourceController] valueForKey:@"collectionView"];
    NSRectEdge edge = [self.identifier isEqualToString:@"tolike"] ? NSMinYEdge : NSMaxYEdge;
    [[self sourceController] presentViewController:[self destinationController] asPopoverRelativeToRect:self.relativeRect ofView:view preferredEdge:edge behavior:NSPopoverBehaviorTransient];
}

@end
