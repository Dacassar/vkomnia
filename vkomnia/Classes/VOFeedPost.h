//
//  VOFeedPost.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 16.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VOFeedPost : NSObject
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic, readonly) NSDictionary *authorInfo;
@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) NSUInteger likes;
@property (strong, nonatomic, readonly) NSImage *avatar;
@property (assign, nonatomic) NSInteger itemID;
@property (strong, nonatomic) NSString *postURL;

- (instancetype)initWithAuthorInfo:(NSDictionary*)authorInfo;
@end
