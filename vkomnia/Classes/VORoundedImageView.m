//
//  VORoundedImageView.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VORoundedImageView.h"
#import <QuartzCore/QuartzCore.h>

@implementation VORoundedImageView

- (void)setRounded:(BOOL)rounded
{
    _rounded = rounded;
    if (!self.wantsLayer) {
        [self setWantsLayer:YES];
        [self.layer setMasksToBounds:YES];
    }
    if (rounded) {
        CGRect rect = NSRectToCGRect(self.bounds);
        [self.layer setCornerRadius:MIN(CGRectGetHeight(rect), CGRectGetWidth(rect))/2];
    }
    else {
        [self.layer setCornerRadius:0];
    }
    [self setNeedsDisplay];
}

@end
