//
//  VOTableRowView.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOTableRowView.h"

@implementation VOTableRowView

- (void)drawSelectionInRect:(NSRect)dirtyRect
{
    if (self.selectionColor) {
//        [[NSColor colorWithCalibratedWhite:.65 alpha:1.0] setStroke];
        [self.selectionColor setFill];
        NSBezierPath *selectionPath = [NSBezierPath bezierPathWithRect:dirtyRect];//[NSBezierPath bezierPathWithRoundedRect:selectionRect xRadius:6 yRadius:6];
        [selectionPath fill];
//        [selectionPath stroke];
    }
    else {
        [super drawSelectionInRect:dirtyRect];
    }
}

@end
