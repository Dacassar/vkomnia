//
//  VOLikeCell.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 21.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOLikeCell.h"

NSString *const VOLikePressedNotification = @"VOLikePressedNotification";

@interface VOLikeCell ()
//- (IBAction)clickCell:(id)sender;
@end

@implementation VOLikeCell

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected) {
        [[NSNotificationCenter defaultCenter] postNotificationName:VOLikePressedNotification object:self.representedObject];
    }
    
}
@end
