//
//  VOBackColoredView.m
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import "VOView.h"

@implementation VOView

- (void)drawRect:(NSRect)dirtyRect
{
    if (self.backgroundColor) {
        CGContextRef context = (CGContextRef) [[NSGraphicsContext currentContext] graphicsPort];
        CGContextSetRGBFillColor(context, [self.backgroundColor redComponent],[self.backgroundColor greenComponent],[self.backgroundColor blueComponent],[self.backgroundColor alphaComponent]);
        CGContextFillRect(context, NSRectToCGRect(dirtyRect));
    }
    else {
        [super drawRect:dirtyRect];
    }
}

@end
