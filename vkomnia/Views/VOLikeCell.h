//
//  VOLikeCell.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 21.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern NSString *const VOLikePressedNotification;

@interface VOLikeCell : NSCollectionViewItem

@end
