//
//  VOBackColoredView.h
//  VKOmnia
//
//  Created by Yevgeniy Kratko on 15.01.15.
//  Copyright (c) 2015 Yevgeniy Kratko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VOView : NSView
@property (strong, nonatomic) NSColor *backgroundColor;
@end
